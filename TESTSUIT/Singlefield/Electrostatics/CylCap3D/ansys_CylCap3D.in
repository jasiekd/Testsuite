! ===========================================================================
!  3D model of a cyldinric capacitance, filled with two
!  different dielectrica.
! ===========================================================================

fini
/clear
/filname,CylCap3D
/prep7
! initialize macros for .mesh-interface
init

! ===========================
!  GEOMETRIC DEFINITIONS
! ===========================
l = 1e-2   ! length of capacitor
r1 = 1e-3  ! inner radius
r2 = 2e-3  ! middle radius
r3 = 5e-3  ! outer radius

h = 2e-3   ! average mesh size
!h = 1e-3   ! average mesh size

! ===========================
!  CREATE GEOMETRY
! ===========================
! generate keypoints for rotation
k,1,0,0,0
k,2,0,0,1

! generate keypoints at distinct radii
k,3,r1,0,0
k,4,r2,0,0
k,5,r3,0,0
l,3,4
l,4,5

! perform rotation by 90 deg
lrotat,3,4,5,,,,1,2,90,1
l,6,7
l,7,8

! create areas
a,3,4,7,6
a,4,5,8,7

! ===========================
!  CREATE 2D MESH 
! ===========================

! assign circumferential element size
lesize,3,,,6
lesize,4,,,6
lesize,5,,,6

! area mesh
esize,h/2
mshkey,1
setelems,'quadr','quad'
amesh,1

mshkey,0
esize,h
setelems,'triangle','quad'
amesh,2

! ===========================
!  EXTRUDE TO 3D MESH
! ===========================
esize,,l/h
setelems,'brick','quad'
vext,all,,,,,l

! clear surface mesh so far
allsel
aclear,all

! generate surface mesh
csys,1
asel,s,loc,x,r1
cm,a_inner,area
asel,s,loc,x,r3
cm,a_outer,area

csys,0
cmsel,s,a_inner
cmsel,a,a_outer
setelems,'quadr','quad'
amesh,all

! IMPORTANT: compress element number
allsel
numcmp,elem

! ===========================
!  WRITE MESH
! ===========================
csys,1
vsel,s,loc,x,r1,r2
eslv
welems,'dielec-1'

vsel,s,loc,x,r2,r3
eslv
welems,'dielec-2'

cmsel,s,a_inner
esla
welems,'inner'

cmsel,s,a_outer
esla
welems,'outer'


! write all nodes
allsel
wnodes

! make .hdf5 file
mkmesh
