<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/master/CFS-Simulation/CFS.xsd">
 
 <documentation>
   <title>SemiSphere</title>
   <authors>
     <author>ftoth</author>
   </authors>
   <date>2020-04-08</date>
   <keywords>
     <keyword>mechanic</keyword>
     <keyword>pml</keyword>
   </keywords>
   <references>
     Toth, F. and Kaltenbacher, M.
     Fully coupled linear modelling of incompressible free-surface flow, compressible air and flexible structures
     International Journal for Numerical Methods in Engineerin; 107(11); https://doi.org/10.1002/nme.5194
   </references>
   <isVerified>yes</isVerified>
   <description>
     Heave case from Section 4.4 of above reference.
   </description>
 </documentation>
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="semisphere.msh"></gmsh>-->
      <hdf5 fileName="SemiSphereHeave.h5ref"/>
    </input>
    <output>
      <hdf5/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="WATER" material="water"/>
      <region name="DISK" material="disk"/>
      <region name="PML-SIDE" material="water"/>
      <?region name="PML-BOTTOM" material="water"/?>
    </regionList>
    <surfRegionList>
      <surfRegion name="SURFACE-WATER"/>
      <surfRegion name="SURFACE-PML"/>
      <surfRegion name="WATER-DISK"/>
      <surfRegion name="DISK-TOP"/>
    </surfRegionList>
  </domain> 

  <sequenceStep index="1">
    <analysis>
     <harmonic>
        <numFreq>  7 </numFreq>
        <startFreq> 0.1 </startFreq>
        <stopFreq>  0.7 </stopFreq>
        <sampling> linear </sampling>
      </harmonic> 
    </analysis>
    <pdeList>
      <waterWave> 
        <regionList>
          <region name="WATER"/>
          <region name="PML-SIDE">
            <transforms>
              <id name="myPML"/>
            </transforms>
          </region>
        </regionList>
        <transformList>
          <pml id="myPML">
            <type>inverseDist</type>
            <dampFactor>5</dampFactor>
          </pml>
        </transformList>
        <bcsAndLoads>
          <freeSurfaceCondition name="SURFACE-WATER" volumeRegion="WATER"/>
          <freeSurfaceCondition volumeRegion="PML-SIDE" name="SURFACE-PML"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="waterPressure" complexFormat="realImag">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </waterWave>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="DISK"/>
        </regionList>
        <bcsAndLoads>
          <pressure name="DISK-TOP" value="1.0"> </pressure>
          <fix name="DISK-SYM"><comp dof="x"/></fix>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement" complexFormat="realImag">
            <allRegions/>
          </nodeResult>
          <elemResult type="mechStress">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
    <couplingList>
      <direct>
        <waterWaveMechDirect>
          <surfRegionList>
            <surfRegion name="WATER-DISK"/>
          </surfRegionList>
        </waterWaveMechDirect>
      </direct>
    </couplingList>
    <linearSystems>
            <system>
              <solutionStrategy>
                <standard>
                  <matrix storage="sparseNonSym" reordering="noReordering"/>
                </standard>
              </solutionStrategy>
              <solverList>
                <pardiso/><!-- needs pardiso, direct LU is incredlibly slow -->
              </solverList>
            </system>
   </linearSystems>
  </sequenceStep>
</cfsSimulation>
