function[s] = square(val)
  s = val^2;
end